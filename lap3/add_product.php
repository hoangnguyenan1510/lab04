<?php
require_once("entities/product.class.php");
if(isset($_POST["btnsubmit"])){
	$productName = $_POST["txtName"];
	$cateID = $_POST["txtCateID"];
	$price = $_POST["txtprice"];
	$quantity = $_POST["txtquantity"];
	$description = $_POST["txtdesc"];
	$picture = $_POST["txtpic"];
	
	$newProduct = new Product($productName, $cateID, $price, $quantity, $description, $picture);
	$result = $newProduct->save();
	if(!$result)
	{
		header("Location: add_product.php?failure");		
	}
	else{
		header("Location: add_product.php?inserted");	
	}
}
?>
<?php
	if(isset($_GET["inserted"])){
		echo "<h2>Them san pham thanh cong</h2>";
	}
?>
<form method ="post">
	<div class="row">
		<div class="lbltitle">
			<label>Ten san pham</label>
		</div>
		<div class="lblinput">
			<input type="text" name="txtName" value="<?php echo isset($_POST["txtName"]) ? $_POST["txtName"] :"" ;?>" />
		</div>
	</div>
	<div class="row">
		<div class="lbltitle">
			<label>Mo ta san pham</label>
		</div>
		<div class="lblinput">
			<textarea  name="txtdesc" cols="21" rows="10" value="<?php echo isset($_POST["txtdesc"]) ? $_POST["txtdesc"] :"" ;?>"> </textarea>
		</div>
	</div>
	<div class="row">
		<div class="lbltitle">
			<label>So luong san pham</label>
		</div>
		<div class="lblinput">
			<input type="number" name="txtquantity" value="<?php echo isset($_POST["txtquantity"]) ? $_POST["txtquantity"] :"" ;?>" />
		</div>
	</div>
	<div class="row">
		<div class="lbltitle">
			<label>Gia san pham</label>
		</div>
		<div class="lblinput">
			<input type="float" name="txtprice" value="<?php echo isset($_POST["txtprice"]) ? $_POST["txtprice"] :"" ;?>" />
		</div>
	</div>
	<div class="row">
		<div class="lbltitle">
			<label>Loai san pham</label>
		</div>
		<div class="lblinput">
			<input type="text" name="txtCateID" value="<?php echo isset($_POST["txtCateID"]) ? $_POST["txtCateID"] :"" ;?>" />
		</div>
	</div>
	<div class="row">
		<div class="lbltitle">
			<label>Hinh anh</label>
		</div>
		<div class="lblinput">
			<input type="picture" name="txtpic" value="<?php echo isset($_POST["txtpic"]) ? $_POST["txtpic"] :"" ;?>" />
		</div>
	</div>
	<div class="row">
		<div class="submit">
			<input type="submit" name="btnsubmit" value="Them san pham"> 
		</div>
	</div>
</form>
<?php include_once("footer.php"); ?>
	

